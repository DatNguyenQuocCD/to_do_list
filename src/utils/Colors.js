export const COLORS = {
  whiteColor: '#FFFFFF',
  blackColor: '#000000',
  pinkColor: '#C688EB',
  blueColor: '#593E67',
  brickColor: '#FEA837',
  greenColor: '#4EB09B',
  redColor: '#F28076',
  yellowColor: '#FFFDA1',
  purpleColor: '#84E6F8',
  brownColor: '#EFBF7F',
};

export const COLORSTABLE = [
  {color: COLORS.whiteColor},
  {color: COLORS.purpleColor},
  {color: COLORS.yellowColor},
  {color: COLORS.brownColor},
];
