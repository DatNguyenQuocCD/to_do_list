import {CheckLength} from './CheckLength';

export const ToDay = () => {
  let date = new Date();
  return (
    date.getFullYear() +
    '-' +
    CheckLength(date.getMonth() + 1) +
    '-' +
    CheckLength(date.getDate())
  );
};
