import {combineReducers} from 'redux';
import {getTaskSlice} from './tasks/slices';

const rootReducer = combineReducers({
  getTasksToolKitReducer: getTaskSlice.reducer,
});

export default rootReducer;
