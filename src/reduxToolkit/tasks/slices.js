import {createSlice} from '@reduxjs/toolkit';
import {getTasksData} from './thunk';

const initialState = {
  data: [],
  loading: false,
  error: [],
};

export const getTaskSlice = createSlice({
  name: 'task',
  initialState,
  extraReducers: builder => {
    builder.addCase(getTasksData.pending, (state, action) => {
      console.log('💩: action', action);
      state.loading = true;
    });
    builder.addCase(getTasksData.fulfilled, (state, action) => {
      console.log('💩: action', action);
      state.loading = false;
      console.log('payload => ', action?.payload);
      state.data.push(...action?.payload?.data);
    });
    builder.addCase(getTasksData.rejected, (state, action) => {
      console.log('💩: action', action);
      state.loading = false;
    });
  },
});

export const {addTask} = getTaskSlice.actions;
export const selectTask = state => state.task;
export default getTaskSlice.reducer;
