import {createAsyncThunk} from '@reduxjs/toolkit';
import {getTasksApi} from '../../api/tasks';

export const getTasksData = createAsyncThunk('task/getTasksData', async () => {
  const response = await getTasksApi({limit: 10, page: 1});
  return response;
});
