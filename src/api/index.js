import axios from 'axios';

const ROOT_API = 'https://642b977fd7081590f9254204.mockapi.io/';

const axiosRequest = axios.create({
  baseURL: ROOT_API,
  responseType: 'json',
  withCredentials: true,
  timeout: 15000,
});

axiosRequest.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    return Promise.reject(error);
  },
);

axiosRequest.interceptors.response.use(
  response => {
    return response;
  },
  async (error = {}) => {
    console.log('💩: error', error);
  },
);

export default axiosRequest;
