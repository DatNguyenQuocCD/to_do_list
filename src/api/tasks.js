import axiosRequest from './index';

const URL = 'Tasks';
export async function getTasks() {
  return axiosRequest.get(URL);
}

export async function addNewTask(data) {
  return axiosRequest.post(URL, data);
}

export async function updateTask(id, data) {
  let url = `${URL}/${id}`;
  return axiosRequest.put(url, data);
}
