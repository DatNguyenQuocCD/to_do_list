import React, {useEffect, useState} from 'react';
import {
  FlatList,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {getTasks} from '../../api/tasks';
import ButtonDelete from '../../components/buttonDelete/ButtonDelete';
import ButtonNewAddTask from '../../components/buttonNewAddTask/ButtonNewAddTask';
import TaskInDay from '../../components/taskInDay/TaskInDay';
import {COLORS} from '../../utils/Colors';

const Home = ({navigation}) => {
  const [tasks, setTasks] = useState([]);
  const [idTask, setIdtask] = useState(null);
  const [isDelete, setIsDelete] = useState(false);
  // const dispatch = useDispatch();
  // const myTasksToolkit = useSelector(state => state.getCabinetsToolKitReducer);

  // useEffect(() => {
  //   dispatch(getTasksData());
  // }, []);

  const getData = async () => {
    try {
      const res = await getTasks();
      setTasks(res?.data);
    } catch (error) {
      console.log('💩: getData -> error', error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  console.log('tasks: ', tasks);

  const buttonHandle = () => {
    if (idTask === null) {
      return (
        <ButtonNewAddTask onPress={() => navigation.navigate('AddTask')} />
      );
    } else {
      return (
        <ButtonDelete
          onPress={async () => {
            await setIsDelete(true);
            await setIsDelete(false);
          }}
        />
      );
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>My Task</Text>
      <SafeAreaView>
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={tasks}
          renderItem={({item}) => (
            <TaskInDay
              content={item.content}
              time={item.date}
              status={item.status}
              color={item.color}
              idDelete={idTask}
              id={item.id}
              isDelete={isDelete}
              onPress={() =>
                navigation.navigate('ShowDetailTask', {
                  idTask: item.id,
                  contentTask: item.content,
                  colorTask: item.color,
                  dateTask: item.date,
                  statusTask: item.status,
                })
              }
              onLongPress={() => {
                if (idTask === null) {
                  setIdtask(item.id);
                } else if (idTask != item.id) {
                  setIdtask(item.id);
                } else {
                  setIdtask(null);
                }
              }}
            />
          )}
          keyExtractor={item => item.id}
          refreshControl={
            <RefreshControl
              refreshing={false}
              onRefresh={() => {
                getData();
              }}
            />
          }
        />
      </SafeAreaView>
      {buttonHandle()}
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    height: 850,
    backgroundColor: COLORS.pinkColor,
  },
  title: {
    color: COLORS.whiteColor,
    fontSize: 30,
    fontWeight: '900',
    alignSelf: 'center',
    marginTop: 50,
  },
});
