import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {updateTask} from '../../api/tasks';
import Button from '../../components/button/Button';
import ButtonBack from '../../components/buttonBack/ButtonBack';
import ColorsTable from '../../components/colorsTable/ColorsTable';
import Input from '../../components/input/Input';
import ModalTime from '../../components/modal/Modal';
import Status from '../../components/status/Status';
import {COLORS} from '../../utils/Colors';

const ShowDetailTask = ({route, navigation}) => {
  const {idTask, contentTask, colorTask, dateTask, statusTask} = route.params;
  const [selected, setSelected] = useState(dateTask);

  const [task, setTask] = useState(contentTask);
  const [status, setStatus] = useState(statusTask);
  const [color, setColor] = useState(colorTask);

  const updateTaskHandler = async () => {
    await updateTask(idTask, {
      date: selected,
      status,
      content: task,
      color,
    });
    navigation.goBack();
  };
  return (
    <View style={styles.container}>
      <View style={[styles.modal, {backgroundColor: color}]}>
        <ButtonBack onPress={() => navigation.goBack()} />
        <Text style={styles.title}>Detail Task</Text>
        <View style={styles.content}>
          <ModalTime
            selected={selected}
            setSelected={setSelected}
            time={selected}
          />
        </View>
        <Input
          placeholder={'Enter describe your task'}
          setData={setTask}
          value={task}
        />
        <Status
          status={status}
          bottom={330}
          onPress={() => {
            if (status === 'To do') {
              setStatus('Doing');
            } else if (status === 'Doing') {
              setStatus('Done');
            } else {
              setStatus('To do');
            }
          }}
        />
        <View style={{marginTop: 50}}>
          <ColorsTable setColor={setColor} />
        </View>
        <Button
          title={'Update'}
          color={COLORS.greenColor}
          onPress={updateTaskHandler}
        />
      </View>
    </View>
  );
};

export default ShowDetailTask;

const styles = StyleSheet.create({
  container: {
    height: 850,
    backgroundColor: COLORS.pinkColor,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal: {
    width: 300,
    height: 600,
    borderRadius: 10,
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: '700',
    color: COLORS.blueColor,
    marginTop: 50,
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
