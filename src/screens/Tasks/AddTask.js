import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {addNewTask} from '../../api/tasks';
import Button from '../../components/button/Button';
import ButtonBack from '../../components/buttonBack/ButtonBack';
import ColorsTable from '../../components/colorsTable/ColorsTable';
import Input from '../../components/input/Input';
import ModalTime from '../../components/modal/Modal';
import {COLORS} from '../../utils/Colors';
import {ToDay} from '../../utils/SetTime';

const AddTask = ({navigation}) => {
  const [selected, setSelected] = useState(ToDay);

  const [task, setTask] = useState('');
  const [color, setColor] = useState(COLORS.whiteColor);

  const addNewTaskHandler = async () => {
    try {
      await addNewTask({
        date: selected,
        status: 'To do',
        content: task,
        color,
      });
      setSelected(ToDay);
      setTask('');
      setColor(COLORS.whiteColor);
      navigation.goBack();
    } catch (e) {
      console.log('error: ', e);
    }
  };
  return (
    <View style={styles.container}>
      <View style={[styles.modal, {backgroundColor: color}]}>
        <ButtonBack onPress={() => navigation.goBack()} />
        <Text style={styles.title}>Add Task</Text>
        <View style={styles.content}>
          <ModalTime
            selected={selected}
            setSelected={setSelected}
            time={selected}
          />
        </View>
        <Input
          placeholder={'Enter describe your task'}
          setData={setTask}
          value={task}
        />
        <ColorsTable setColor={setColor} />
        <Button
          title={'Add Task'}
          color={COLORS.greenColor}
          onPress={addNewTaskHandler}
        />
      </View>
    </View>
  );
};

export default AddTask;

const styles = StyleSheet.create({
  container: {
    height: 850,
    backgroundColor: COLORS.pinkColor,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal: {
    width: 300,
    height: 600,
    borderRadius: 10,
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {fontSize: 20, fontWeight: '700', color: COLORS.blueColor, margin: 50},
  content: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
