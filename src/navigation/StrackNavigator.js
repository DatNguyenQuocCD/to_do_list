import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {StyleSheet} from 'react-native';
import Home from '../screens/Home';
import AddTask from '../screens/Tasks/AddTask';
import ShowDetailTask from '../screens/Tasks/ShowDetailTask';

const Stack = createNativeStackNavigator();

const StrackNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={'Home'}>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AddTask"
          component={AddTask}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ShowDetailTask"
          component={ShowDetailTask}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default StrackNavigator;

const styles = StyleSheet.create({});
