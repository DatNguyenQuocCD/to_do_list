import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {COLORS} from '../../utils/Colors';

const ButtonNewAddTask = props => {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <Icon name="pluscircle" color={COLORS.whiteColor} size={60} />
    </TouchableOpacity>
  );
};

export default ButtonNewAddTask;

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    marginTop: 80,
  },
});
