import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {COLORS} from '../../utils/Colors';

const Status = props => {
  const status = props.status;
  var buttonColor = COLORS.redColor;
  if (status === 'Doing') {
    buttonColor = COLORS.brickColor;
  } else if (status === 'Done') {
    buttonColor = COLORS.greenColor;
  }
  return (
    <TouchableOpacity
      style={[
        styles.status,
        {backgroundColor: buttonColor, bottom: props.bottom},
      ]}
      onPress={props.onPress}>
      <Text style={styles.valueStatus}>{status}</Text>
    </TouchableOpacity>
  );
};

export default Status;

const styles = StyleSheet.create({
  status: {
    position: 'absolute',
    alignSelf: 'center',
    width: 100,
    height: 30,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  valueStatus: {
    fontSize: 15,
    fontWeight: '700',
    color: COLORS.blackColor,
  },
});
