import React, {useEffect, useRef} from 'react';
import {Animated, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Tick from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/Entypo';
import {COLORS} from '../../utils/Colors';
import Status from '../status/Status';

const TaskInDay = props => {
  const backgroundColor = props.color;
  const id = props.id;
  const idDelete = props.idDelete;
  const isdDelete = props.isDelete;
  const locationMotion = useRef(new Animated.Value(-5)).current;
  const opacityMotion = useRef(new Animated.Value(1)).current;
  const topMotion = useRef(new Animated.Value(50)).current;
  const containerOpacityMotion = useRef(new Animated.Value(1)).current;
  const widthMotion = useRef(new Animated.Value(250)).current;
  const marginRightMotion = useRef(new Animated.Value(30)).current;
  const centerMotion = useRef(new Animated.Value(0)).current;

  useEffect(() => {}, []);
  let opacity = 1;
  let opacityTick = 0;
  if (id === idDelete) {
    opacity = 0.3;
    opacityTick = 1;
    Animated.parallel([
      Animated.timing(locationMotion, {
        toValue: -80,
        duration: 800,
      }),
      Animated.timing(opacityMotion, {
        toValue: 0,
        duration: 800,
      }),
    ]).start();
  } else {
    Animated.parallel([
      Animated.timing(locationMotion, {
        toValue: -5,
        duration: 1500,
      }),
      Animated.timing(opacityMotion, {
        toValue: 1,
        duration: 1500,
      }),
    ]).start();
  }

  if (id === idDelete && isdDelete) {
    Animated.parallel([
      Animated.timing(topMotion, {
        toValue: 300,
        duration: 800,
      }),
      Animated.timing(containerOpacityMotion, {
        toValue: 0,
        duration: 800,
      }),
      Animated.timing(widthMotion, {
        toValue: 0,
        duration: 500,
      }),
      Animated.timing(marginRightMotion, {
        toValue: 0,
        duration: 500,
      }),
      Animated.timing(centerMotion, {
        toValue: 150,
        duration: 500,
      }),
    ]).start();
  }

  return (
    <Animated.View
      style={[
        {
          zIndex: 1000,
          marginTop: topMotion,
          opacity: containerOpacityMotion,
          width: widthMotion,
          marginRight: marginRightMotion,
          left: centerMotion,
        },
      ]}>
      <View style={[styles.iconTick, {opacity: opacityTick}]}>
        <Tick name="checkcircleo" color={COLORS.purpleColor} size={30} />
      </View>
      <TouchableOpacity onPress={props.onPress} onLongPress={props.onLongPress}>
        <Animated.View
          style={[
            styles.container,
            {
              backgroundColor: backgroundColor,
              opacity: opacity,
              width: widthMotion,
            },
          ]}>
          <Animated.View
            style={[
              styles.iconPin,
              {
                top: locationMotion,
                right: locationMotion,
                opacity: opacityMotion,
              },
            ]}>
            <Icon name="pin" color={COLORS.blueColor} size={30} />
          </Animated.View>
          <Text style={styles.time}>{props.time}</Text>
          <Text style={styles.content}>{props.content}</Text>
          <Status status={props.status} bottom={50} />
        </Animated.View>
      </TouchableOpacity>
    </Animated.View>
  );
};

export default TaskInDay;

const styles = StyleSheet.create({
  container: {
    height: 400,
    borderRadius: 20,
    paddingVertical: 45,
    paddingHorizontal: 25,
    margin: 30,
    marginBottom: 80,
  },
  iconTick: {
    position: 'absolute',
    top: 35,
    left: 35,
    zIndex: 100,
  },
  iconPin: {
    position: 'absolute',
  },
  time: {
    color: COLORS.blackColor,
    fontSize: 15,
  },
  content: {
    marginTop: 30,
  },
});
