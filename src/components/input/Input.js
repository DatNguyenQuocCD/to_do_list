import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import {COLORS} from '../../utils/Colors';

const Input = props => {
  return (
    <View style={styles.container}>
      <TextInput
        placeholder={props.placeholder}
        value={props.value}
        onChangeText={newText => props.setData(newText)}
        secureTextEntry={props.status}
        keyboardType={props.type}
        style={{marginTop: 10, width: 200}}
        maxLength={props.length}
      />
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  container: {
    borderBottomColor: COLORS.blueColor,
    borderBottomWidth: 3,
    marginBottom: 22,
    marginTop: 11,
  },
});
