import React from 'react';
import {FlatList, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {COLORS, COLORSTABLE} from '../../utils/Colors';
import Color from './Color';

const ColorsTable = props => {
  const Data = COLORSTABLE;
  return (
    <View style={styles.container}>
      <Text style={styles.title}>ColorsTable</Text>
      <SafeAreaView>
        <FlatList
          data={Data}
          renderItem={({item}) => (
            <Color
              color={item.color}
              onPress={() => {
                props.setColor(item.color);
              }}
            />
          )}
          keyExtractor={item => item.id}
          numColumns={3}
        />
      </SafeAreaView>
    </View>
  );
};

export default ColorsTable;

const styles = StyleSheet.create({
  container: {
    width: 200,
  },
  title: {
    color: COLORS.blueColor,
    marginTop: 10,
    marginBottom: 20,
  },
});
