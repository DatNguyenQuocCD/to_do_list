import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {COLORS} from '../../utils/Colors';

const Color = props => {
  const color = props.color;
  return (
    <TouchableOpacity
      style={[styles.container, {backgroundColor: color}]}
      onPress={props.onPress}>
      <Text>{props.name}</Text>
    </TouchableOpacity>
  );
};

export default Color;

const styles = StyleSheet.create({
  container: {
    width: 50,
    height: 50,
    borderRadius: 100,
    marginRight: 25,
    marginBottom: 25,
    borderBottomColor: COLORS.blueColor,
    borderWidth: 1,
  },
});
