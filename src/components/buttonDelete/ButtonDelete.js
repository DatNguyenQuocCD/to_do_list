import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {COLORS} from '../../utils/Colors';

const ButtonDelete = props => {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <Icon name="delete" color={COLORS.whiteColor} size={60} />
    </TouchableOpacity>
  );
};

export default ButtonDelete;

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    position: 'absolute',
    top: 700,
  },
});
