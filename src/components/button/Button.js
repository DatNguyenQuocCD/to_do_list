import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {COLORS} from '../../utils/Colors';

const Button = props => {
  return (
    <TouchableOpacity
      style={[styles.container, {backgroundColor: props.color}]}
      onPress={props.onPress}>
      <Text style={styles.title}>{props.title}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 80,
    height: 40,
    borderRadius: 5,
  },
  title: {
    color: COLORS.whiteColor,
    fontSize: 15,
    fontWeight: '700',
  },
});
