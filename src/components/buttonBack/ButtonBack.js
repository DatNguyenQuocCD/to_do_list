import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {COLORS} from '../../utils/Colors';

const ButtonBack = props => {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <Icon name="closecircleo" color={COLORS.blueColor} size={30} />
    </TouchableOpacity>
  );
};

export default ButtonBack;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    right: 10,
    top: 10,
  },
});
