import {createNativeStackNavigator} from '@react-navigation/native-stack';
import * as React from 'react';
import {Provider} from 'react-redux';
import StrackNavigator from './src/navigation/StrackNavigator';
import {store} from './src/reduxToolkit/store';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <Provider store={store}>
      <StrackNavigator />
    </Provider>
  );
}

export default App;
